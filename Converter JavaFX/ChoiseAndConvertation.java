package sample;

import java.text.DecimalFormat;

public class ChoiseAndConvertation {

    static DecimalFormat df = new DecimalFormat("###.####");

    public static String timeConvert(String convertFrom, String convertTo, Double firstNumber, Double secondNumber, Boolean firstNumberToSecond) {

        switch (convertTo) {
            case "min":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 60));
                else
                    return String.valueOf(df.format(secondNumber * 60));
            case "hour":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / (60 * 60)));
                else
                    return String.valueOf(df.format(secondNumber * (60 * 60)));
            case "day":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / (24 * 60 * 60)));
                else
                    return String.valueOf(df.format(secondNumber * (24 * 60 * 60)));
            case "week":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / (7 * 24 * 60 * 60)));
                else
                    return String.valueOf(df.format(secondNumber * (7 * 24 * 60 * 60)));
            case "month":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / (30 * 24 * 60 * 60)));
                else
                    return String.valueOf(df.format(secondNumber * (30 * 24 * 60 * 60)));
            case "astronomical year":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / (365 * 24 * 60 * 60)));
                else
                    return String.valueOf(df.format(secondNumber * (365 * 24 * 60 * 60)));
            case "third":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / (3 * 30 * 24 * 60 * 60)));
                else
                    return String.valueOf(df.format(secondNumber * (3 * 30 * 24 * 60 * 60)));
            default:
                return "Идентичные величины конвертации!";
        }
    }

    public static String weightConvert(String convertFrom, String convertTo, Double firstNumber, Double secondNumber, Boolean firstNumberToSecond) {

        switch (convertTo) {
            case "g":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 1000));
                else
                    return String.valueOf(df.format(secondNumber / 1000));
            case "c":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 100));
                else
                    return String.valueOf(df.format(secondNumber / 100));
            case "carat":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 5000));
                else
                    return String.valueOf(df.format(secondNumber / 5000));
            case "eng pound":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 2.20462));
                else
                    return String.valueOf(df.format(secondNumber / 2.20462));
            case "pound":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 0.37324));
                else
                    return String.valueOf(df.format(secondNumber * 0.37324));
            case "stone":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 6.35029));
                else
                    return String.valueOf(df.format(secondNumber * 6.35029));
            case "rus pound":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 2.441933));
                else
                    return String.valueOf(df.format(secondNumber * 2.441933));
            default:
                return "Идентичные величины конвертации!";
        }
    }

    public static String volumeConvert(String convertFrom, String convertTo, Double firstNumber, Double secondNumber, Boolean firstNumberToSecond) {

        switch (convertTo) {
            case "m^3":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 1000));
                else
                    return String.valueOf(df.format(secondNumber * 1000));
            case "gallon":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 3.78541));
                else
                    return String.valueOf(df.format(secondNumber * 3.78541));
            case "pint":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 2.11338));
                else
                    return String.valueOf(df.format(secondNumber / 2.11338));
            case "quart":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 1.05669));
                else
                    return String.valueOf(df.format(secondNumber / 1.05669));
            case "barrel":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 0.006289810767583661));
                else
                    return String.valueOf(df.format(secondNumber / 0.006289810767583661));
            case "cubic foot":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 28.3168));
                else
                    return String.valueOf(df.format(secondNumber * 28.3168));
            case "cubic inch":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 61.0237));
                else
                    return String.valueOf(df.format(secondNumber / 61.0237));
            default:
                return "Идентичные величины конвертации!";
        }
    }

    public static String lenthConvert(String convertFrom, String convertTo, Double firstNumber, Double secondNumber, Boolean firstNumberToSecond) {

        switch (convertTo) {
            case "km":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 1000));
                else
                    return String.valueOf(df.format(secondNumber * 1000));
            case "mile":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 1609.34));
                else
                    return String.valueOf(df.format(secondNumber * 1609.34));
            case "nautical mile":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 1852));
                else
                    return String.valueOf(df.format(secondNumber * 1852));
            case "cable":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 185.2));
                else
                    return String.valueOf(df.format(secondNumber * 185.2));
            case "league":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 5556));
                else
                    return String.valueOf(df.format(secondNumber * 5556));
            case "foot":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 0.3048));
                else
                    return String.valueOf(df.format(secondNumber * 0.3048));
            case "yard":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber / 0.9144));
                else
                    return String.valueOf(df.format(secondNumber * 0.9144));
            default:
                return "Идентичные величины конвертации!";
        }
    }

    public static String temperatureConvert(String convertFrom, String convertTo, Double firstNumber, Double secondNumber, Boolean firstNumberToSecond) {

        switch (convertTo) {
            case "K (Шкала Кельвина)":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber + 273.15));
                else
                    return String.valueOf(df.format(secondNumber - 273.15));
            case "Re (Шкала Реомюра)":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 1.25));
                else
                    return String.valueOf(df.format(secondNumber / 1.25));
            case "F (Шкала Фаренгейта)":
                if (firstNumberToSecond)
                    return String.valueOf(df.format((firstNumber * 1.8) + 32));
                else
                    return String.valueOf(df.format((secondNumber - 32) / 1.8));
            case "Ro (Шкала Рёмера)":
                if (firstNumberToSecond)
                    return String.valueOf(df.format((firstNumber * 21 / 40) + 7.5));
                else
                    return String.valueOf(df.format((secondNumber - 7.5) * 40 / 21));
            case "Ra (Шкала Ранкина)":
                if (firstNumberToSecond)
                    return String.valueOf(df.format((firstNumber * 1.8) + 32 + 459.67));
                else
                    return String.valueOf(df.format((secondNumber - 32 - 459.67) / 1.8));
            case "N (Шкала Ньютона)":
                if (firstNumberToSecond)
                    return String.valueOf(df.format(firstNumber * 100 / 33));
                else
                    return String.valueOf(df.format(secondNumber * 33 / 100));
            case "D (Шкала Дели́ля)":
                if (firstNumberToSecond)
                    return String.valueOf(df.format((100 - firstNumber) * 3 / 2));
                else
                    return String.valueOf(df.format(100 - secondNumber * 2 / 3));
            default:
                return "Идентичные величины конвертации!";
        }
    }
}

