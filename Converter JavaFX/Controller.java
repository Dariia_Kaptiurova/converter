package sample;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    ComboBox magnitude = new ComboBox<>();

    @FXML
    ComboBox convertFrom = new ComboBox<>();

    @FXML
    ComboBox convertTo = new ComboBox<>();

    @FXML
    TextField value1;

    @FXML
    TextField value2;

    @FXML
    public void initialize() {
        magnitude.getItems().setAll(Magnitudes.values());

        magnitude.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertFrom.setValue(Magnitudes.getUnit0((Magnitudes) newValue)));

        magnitude.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertTo.getItems().setAll(Magnitudes.getAllUnits((Magnitudes) newValue)));
        magnitude.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertTo.setValue(Magnitudes.getUnit1((Magnitudes) newValue)));

    }

    @FXML
    private void changeCategoryAction() {
        value2.setText("");
        value1.setText("");
    }

    @FXML
    private void convert() {
        Boolean firstNumToSecondNum = false;

        String categoryOfMetrics = parseComBoBox(magnitude);

        String firstMetric = parseComBoBox(convertFrom);
        String secondMetric = parseComBoBox(convertTo);

        double firstNum = 0;
        double secondNum = 0;

        if (value2.isFocused()) {
            firstNum = parseTextField(value2);
            firstNumToSecondNum = true;
        } else {
            secondNum = parseTextField(value1);
        }

        switch (categoryOfMetrics) {
            case "LENGTH":
                if (firstNumToSecondNum) {
                    value1.setText(ChoiseAndConvertation.lenthConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    value2.setText(ChoiseAndConvertation.lenthConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "TEMPERATURE":
                if (firstNumToSecondNum) {
                    value1.setText(ChoiseAndConvertation.temperatureConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    value2.setText(ChoiseAndConvertation.temperatureConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "WEIGHT":
                if (firstNumToSecondNum) {
                    value1.setText(ChoiseAndConvertation.weightConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    value2.setText(ChoiseAndConvertation.weightConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "TIME":
                if (firstNumToSecondNum) {
                    value1.setText(ChoiseAndConvertation.timeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    value2.setText(ChoiseAndConvertation.timeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "VOLUME":
                if (firstNumToSecondNum) {
                    value1.setText(ChoiseAndConvertation.volumeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    value2.setText(ChoiseAndConvertation.volumeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
        }
    }

    @FXML
    public Double parseTextField(TextField textField) {

        try {
            Double number = Double.parseDouble(textField.getText());
            return number;
        } catch (Exception e) {
            return 0.0;
        }
    }

    @FXML
    public String parseComBoBox(ComboBox comboBox) {

        String unitValue = comboBox.getValue().toString();

        return unitValue;
    }
}


