package sample;

import java.util.ArrayList;

public enum Magnitudes {
    TIME("sec", "min", "hour", "day", "week", "month", "astronomical year", "third"),
    WEIGHT("kg", "g", "c", "carat", "eng pound", "pound", "stone", "rus pound"),
    VOLUME("l", "m3", "gallon", "pint", "quart", "barrel", "cubic foot", "cubic inch"),
    LENGTH("m", "km", "mile", "nautical mile", "cable", "league", "foot", "yard"),
    TEMPERATURE("C", "K (Шкала Кельвина)", "F (Шкала Фаренгейта)", "Re (Шкала Реомюра)", "Ro (Шкала Рёмера)", "Ra (Шкала Ранкина)", "N (Шкала Ньютона)", "D (Шкала Дели́ля)");

    public static String getUnit0(Magnitudes category) {
        String unit0 = category.unit0;
        return unit0;
    }

    public static String getUnit1(Magnitudes category) {
        String unit1 = category.unit1;
        return unit1;
    }

    public static ArrayList getAllUnits(Magnitudes measurement) {
        ArrayList allUnits = new ArrayList();

        allUnits.add(measurement.unit0);
        allUnits.add(measurement.unit1);
        allUnits.add(measurement.unit2);
        allUnits.add(measurement.unit3);
        allUnits.add(measurement.unit4);
        allUnits.add(measurement.unit5);
        allUnits.add(measurement.unit6);
        allUnits.add(measurement.unit7);


        return allUnits;
    }


    Magnitudes(String unit0, String unit1, String unit2, String unit3, String unit4, String unit5, String unit6, String unit7) {
        this.unit0 = unit0;
        this.unit1 = unit1;
        this.unit2 = unit2;
        this.unit3 = unit3;
        this.unit4 = unit4;
        this.unit5 = unit5;
        this.unit6 = unit6;
        this.unit7 = unit7;
    }

    private String unit0;
    private String unit1;
    private String unit2;
    private String unit3;
    private String unit4;
    private String unit5;
    private String unit6;
    private String unit7;

}

